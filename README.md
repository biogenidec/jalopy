# README #

This is a public repository of a single file (gdoJalopy.xml) that contains the java coding format conventions applied to all GDO java projects by the Jalopy tool.

Please do not remove this repository!

It is deliberately made public so that the xml convention file can be pulled by a maven build process without having to authenticate with a user id against BitBucket.

There is no sensitive data in this formatting xml file.